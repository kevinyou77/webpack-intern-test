
import Vue from 'vue';
import App from './App.vue';
import './styles/index.less';

new Vue({
  el: '#app',
  render: h => h(App),
});